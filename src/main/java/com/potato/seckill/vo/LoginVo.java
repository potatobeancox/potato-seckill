package com.potato.seckill.vo;

import javax.validation.constraints.NotNull;

import com.potato.seckill.validator.IsMobile;
import org.hibernate.validator.constraints.Length;

/**
 * 页面上面传递下来的vo对象
 */
public class LoginVo {
	
	@NotNull
	@IsMobile//自定义验证器
	private String mobile;
	
	@NotNull
	@Length(min=32)
	private String password;
	
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public String toString() {
		return "LoginVo [mobile=" + mobile + ", password=" + password + "]";
	}
}
