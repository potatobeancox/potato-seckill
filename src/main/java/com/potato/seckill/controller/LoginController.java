package com.potato.seckill.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.potato.seckill.result.Result;
import com.potato.seckill.service.SeckillUserService;
import com.potato.seckill.vo.LoginVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 处理登录逻辑的控制器
 */
@Controller
@RequestMapping("/login")
public class LoginController {

	private static Logger log = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private SeckillUserService userService;


    /**
     * 返回login页面
     * @return
     */
    @RequestMapping("/to/login")
    public String toLogin() {
        return "login";
    }

    /**
     * 处理登录请求
     * @param response
     * @param loginVo
     * @return
     */
    @RequestMapping("/do/login")
    @ResponseBody
    public Result<Boolean> doLogin(HttpServletResponse response, @Valid LoginVo loginVo) {
    	log.info(loginVo.toString());
    	//登录
    	userService.login(response, loginVo);
    	return Result.success(true);
    }
}
