package com.potato.seckill.controller;

import com.potato.seckill.domain.User;
import com.potato.seckill.redis.UserKey;
import com.potato.seckill.result.CodeMsg;
import com.potato.seckill.result.Result;
import com.potato.seckill.service.RedisService;
import com.potato.seckill.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("/demo")
public class DemoController {

	@Autowired
	private UserService userService;

	@Autowired
	private RedisService redisService;


	@RequestMapping("/")
	@ResponseBody
	public String home() {
		return "Hello World!";
	}

	//1.rest api json输出 2.页面
	@RequestMapping("/hello")
	@ResponseBody
	public Result<String> hello() {
		return Result.success("hello,imooc");
	}

	@RequestMapping("/helloError")
	@ResponseBody
	public Result<String> helloError() {
		return Result.error(CodeMsg.SERVER_ERROR);
	}

	@RequestMapping("/thymeleaf")
	public String thymeleaf(Model model) {
		model.addAttribute("name", "potato");
		return "hello";
	}

	@RequestMapping("/db/get")
	@ResponseBody
	public Result<User> dbGet() {
		User user = userService.getById(1);
		return Result.success(user);
	}


	@RequestMapping("/db/tx")
	@ResponseBody
	public Result<Boolean> dbTx() {
		userService.tx();
		return Result.success(true);
	}


	@RequestMapping("/redis/get")
	@ResponseBody
	public Result<User> redisGet() {
		User  user  = redisService.get(UserKey.getById, ""+1, User.class);
		return Result.success(user);
	}

	@RequestMapping("/redis/set")
	@ResponseBody
	public Result<Boolean> redisSet() {
		User user  = new User();
		user.setId(1);
		user.setName("1111");
		redisService.set(UserKey.getById, ""+1, user);//UserKey:id1
		return Result.success(true);
	}

}
