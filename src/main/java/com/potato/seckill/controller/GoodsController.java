package com.potato.seckill.controller;

import com.potato.seckill.domain.SeckillUser;
import com.potato.seckill.service.RedisService;
import com.potato.seckill.service.SeckillUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * 处理商品和商品相关页面跳转控制器
 */
@Controller
@RequestMapping("/goods")
public class GoodsController {

	@Autowired
	private SeckillUserService userService;
	
	@Autowired
	private RedisService redisService;

	/**
	 * 方法中的user采用自定义 注入的方式进行注入
	 * @param model
	 * @param user
	 * @return
	 */
    @RequestMapping("/to/list")
    public String list(Model model,SeckillUser user) {
    	model.addAttribute("user", user);
        return "goods_list";
    }
    
}
