package com.potato.seckill;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PotatoSeckillApplication {

	public static void main(String[] args) {
		SpringApplication.run(PotatoSeckillApplication.class, args);
	}
}
