package com.potato.seckill.validator;
import  javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.potato.seckill.util.ValidatorUtil;
import org.apache.commons.lang3.StringUtils;


/**
 * <IsMobile, String> IsMobile说明对应注解的类型，String为修饰字段的类型
 */
public class IsMobileValidator implements ConstraintValidator<IsMobile, String> {

	private boolean required = false;

	/**
	 *	可以获取注解中的信息
	 * @param constraintAnnotation
	 */
	public void initialize(IsMobile constraintAnnotation) {
		required = constraintAnnotation.required();
	}

	/**
	 *
	 * @param value
	 * @param context
	 * @return true 验证成功 false 验证失败
	 */
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if(required) {
			return ValidatorUtil.isMobile(value);
		}else {
			if(StringUtils.isEmpty(value)) {
				return true;
			}else {
				return ValidatorUtil.isMobile(value);
			}
		}
	}

}
