package com.potato.seckill.service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import com.potato.seckill.dao.SeckillUserDao;
import com.potato.seckill.domain.SeckillUser;
import com.potato.seckill.exception.GlobalException;
import com.potato.seckill.redis.SeckillUserKey;
import com.potato.seckill.result.CodeMsg;
import com.potato.seckill.util.MD5Util;
import com.potato.seckill.util.UUIDUtil;
import com.potato.seckill.vo.LoginVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * 处理秒杀用户查询的service
 */
@Service
public class SeckillUserService {


    public static final String COOKI_NAME_TOKEN = "token";

    @Autowired
    private SeckillUserDao seckillUserDao;

    @Autowired
    RedisService redisService;

    /**
     * 通过id获取用户 从数据库中获取用户信息
     * @param id
     * @return
     */
    public SeckillUser getById(long id) {
        return seckillUserDao.getById(id);
    }


    /**
     * 通过token获取用户 从redis中获取用户信息
     * @param response
     * @param token
     * @return
     */
    public SeckillUser getByToken(HttpServletResponse response, String token) {
        if(StringUtils.isEmpty(token)) {
            return null;
        }
        SeckillUser user = redisService.get(SeckillUserKey.token, token, SeckillUser.class);
        //延长有效期
        if(user != null) {
            addCookie(response, token, user);
        }
        return user;
    }


    /**
     * 用户登录
     * @param response
     * @param loginVo   用户登录信息
     * @return 出现异常情况 抛异常给全局异常处理器进行处理
     */
    public boolean login(HttpServletResponse response, LoginVo loginVo) {
        if(loginVo == null) {
            throw new GlobalException(CodeMsg.SERVER_ERROR);
        }
        String mobile = loginVo.getMobile();
        String formPass = loginVo.getPassword();
        //判断手机号是否存在
        SeckillUser user = getById(Long.parseLong(mobile));
        if(user == null) {
            throw new GlobalException(CodeMsg.MOBILE_NOT_EXIST);
        }
        //验证密码
        String dbPass = user.getPassword();
        String saltDB = user.getSalt();
        String calcPass = MD5Util.formPassToDBPass(formPass, saltDB);
        if(!calcPass.equals(dbPass)) {
            throw new GlobalException(CodeMsg.PASSWORD_ERROR);
        }
        // 生成uuid 并放入cookie cookie
        String token = UUIDUtil.uuid();
        addCookie(response, token, user);
        return true;
    }

    /**
     * 向redis中存放用户session信息 并将生成uuid存入cookie
     * @param response
     * @param token     相当于cookie id
     * @param user      存放的用户信息
     */
    private void addCookie(HttpServletResponse response, String token, SeckillUser user) {
        redisService.set(SeckillUserKey.token, token, user);
        Cookie cookie = new Cookie(COOKI_NAME_TOKEN, token);
        cookie.setMaxAge(SeckillUserKey.token.expireSeconds());
        cookie.setPath("/");
        response.addCookie(cookie);
    }

}
