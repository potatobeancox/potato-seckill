package com.potato.seckill.dao;

import com.potato.seckill.domain.SeckillUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;


@Mapper
public interface SeckillUserDao {

	/**
	 * 通过id 查询秒杀用户信息
	 * @param id
	 * @return
	 */
	@Select("select * from seckill_user where id = #{id}")
	public SeckillUser getById(@Param("id") long id);
}
