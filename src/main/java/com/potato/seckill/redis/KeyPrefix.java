package com.potato.seckill.redis;

/**
 * redis key 前缀接口
 */
public interface KeyPrefix {
		
	public int expireSeconds();
	
	public String getPrefix();
	
}
