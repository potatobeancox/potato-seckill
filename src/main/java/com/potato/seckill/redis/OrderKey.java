package com.potato.seckill.redis;

/**
 * 订单的redis前缀
 */
public class OrderKey extends BasePrefix {

	public OrderKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

}
