package com.potato.seckill.redis;

/**
 * 存放用户session使用得key
 */
public class SeckillUserKey extends BasePrefix{

	/**
	 * 秒杀的rediskey过期时间
	 */
	public static final int TOKEN_EXPIRE = 3600*24 * 2;
	private SeckillUserKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}
	public static SeckillUserKey token = new SeckillUserKey(TOKEN_EXPIRE, "tk");
}
